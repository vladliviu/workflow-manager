#pragma checksum "C:\Users\vladl\Desktop\WorkflowManager\workflow-manager\WorkflowManager\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dc3b456e355c1920f483759b7f143f773d3de4e0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\vladl\Desktop\WorkflowManager\workflow-manager\WorkflowManager\Views\_ViewImports.cshtml"
using WorkflowManager;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\vladl\Desktop\WorkflowManager\workflow-manager\WorkflowManager\Views\_ViewImports.cshtml"
using WorkflowManager.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dc3b456e355c1920f483759b7f143f773d3de4e0", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3e490815fea00609ae912db5fe7c1366488d670b", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\vladl\Desktop\WorkflowManager\workflow-manager\WorkflowManager\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<header class=""py-5 bg-image-full"" style=""background-image: url('https://www.theagilityeffect.com/app/uploads/2019/09/00_VINCI-ICONOGRAPHIE_GettyImages-1065377816-1280x680.jpg');"">
    <img class=""img-fluid d-block mx-auto"" src=""https://www.codester.com/static/uploads/items/000/006/6147/icon.png""");
            BeginWriteAttribute("alt", " alt=\"", 345, "\"", 351, 0);
            EndWriteAttribute();
            WriteLiteral(@">
</header>

<section class=""py-5"">
    <div class=""container"">
        <h1>Consultanta</h1>
        <p class=""lead"">Consultanta profesionala in domeniul constructiilor civile.</p>
        <p>Compania noastra ofera consultanta incepand cu arhitectura proiectului, alegerea materialelor, uneltelor si echipe specializate in anumite tipuri de constructii.</p>
    </div>
</section>


<div class=""py-5 bg-image-full"" style=""background-image: url('https://www07.abb.com/images/librariesprovider113/smarter-home/smarterbuilding_header_1183x353.jpg?sfvrsn=9b99113_1');"">

    <div style=""height: 200px;""></div>
</div>

<section class=""py-5"">
    <div class=""container"">
        <h1>Echipa</h1>
        <p class=""lead"">Profesionisti cu experienta in domeniu</p>
        <p>Avem o echipa dinamica foarte bine pregatita gata sa raspunda oricarei provocari.</p>
    </div>
</section>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591

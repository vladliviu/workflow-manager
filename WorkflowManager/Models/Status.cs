﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Status
    {
        public Status()
        {
            Proiectactiv = new HashSet<Proiectactiv>();
        }

        public int Id { get; set; }
        public string Nume { get; set; }
        public string Descriere { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Proiectactiv> Proiectactiv { get; set; }
    }
}

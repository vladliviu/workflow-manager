﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Proiect
    {
        public Proiect()
        {
            Proiectactiv = new HashSet<Proiectactiv>();
        }

        public int Id { get; set; }
        public string Nume { get; set; }
        public string Detalii { get; set; }
        public string Status { get; set; }
        public double? Valoare { get; set; }
        public string Finantare { get; set; }
        public string Avatar { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Proiectactiv> Proiectactiv { get; set; }
    }
}

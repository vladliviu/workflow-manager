﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Materiale
    {
        public Materiale()
        {
            Comandamateriale = new HashSet<Comandamateriale>();
        }

        public int Id { get; set; }
        public string Nume { get; set; }
        public string Descriere { get; set; }
        public double? Pret { get; set; }
        public int? Cantitate { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string Avatar { get; set; }

        public virtual ICollection<Comandamateriale> Comandamateriale { get; set; }
    }
}

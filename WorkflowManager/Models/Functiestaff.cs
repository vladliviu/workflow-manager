﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Functiestaff
    {
        public Functiestaff()
        {
            Staff = new HashSet<Staff>();
        }

        public int Id { get; set; }
        public string Nume { get; set; }
        public string Descriere { get; set; }
        public double? Salariu { get; set; }
        public double? Bonus { get; set; }
        public double? Valoaretichet { get; set; }
        public int? Numartichet { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Staff> Staff { get; set; }
    }
}

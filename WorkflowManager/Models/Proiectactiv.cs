﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Proiectactiv
    {
        public int ProiectId { get; set; }
        public int ClientId { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public int EchipaId { get; set; }
        public int StatusId { get; set; }

        public virtual Client Client { get; set; }
        public virtual Echipa Echipa { get; set; }
        public virtual Proiect Proiect { get; set; }
        public virtual Status Status { get; set; }
    }
}

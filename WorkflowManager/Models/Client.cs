﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Client
    {
        public Client()
        {
            Proiectactiv = new HashSet<Proiectactiv>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string Email { get; set; }
        public string Telefon { get; set; }
        public string Adresa { get; set; }
        public string Avatar { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Proiectactiv> Proiectactiv { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Echipa
    {
        public Echipa()
        {
            Echipament = new HashSet<Echipament>();
            Proiectactiv = new HashSet<Proiectactiv>();
            Staff = new HashSet<Staff>();
        }

        public int Id { get; set; }
        public string Nume { get; set; }
        public string Detalii { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }

        public virtual ICollection<Echipament> Echipament { get; set; }
        public virtual ICollection<Proiectactiv> Proiectactiv { get; set; }
        public virtual ICollection<Staff> Staff { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Echipament
    {
        public int Id { get; set; }
        public string Nume { get; set; }
        public string Detalii { get; set; }
        public double? Pret { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int EchipaId { get; set; }

        public virtual Echipa Echipa { get; set; }
    }
}

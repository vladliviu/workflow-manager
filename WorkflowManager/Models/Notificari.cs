﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Notificari
    {
        public int Id { get; set; }
        public string Nume { get; set; }
        public string Tip { get; set; }
        public string Descriere { get; set; }
        public string Status { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int StaffId { get; set; }

        public virtual Staff Staff { get; set; }
    }
}

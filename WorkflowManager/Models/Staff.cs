﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Staff
    {
        public Staff()
        {
            Comandamateriale = new HashSet<Comandamateriale>();
            Notificari = new HashSet<Notificari>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string Email { get; set; }
        public string Adresa { get; set; }
        public string Telefon { get; set; }
        public string Avatar { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int FunctiestaffId { get; set; }
        public int EchipaId { get; set; }

        public virtual Echipa Echipa { get; set; }
        public virtual Functiestaff Functiestaff { get; set; }
        public virtual ICollection<Comandamateriale> Comandamateriale { get; set; }
        public virtual ICollection<Notificari> Notificari { get; set; }
    }
}

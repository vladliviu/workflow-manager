﻿using System;
using System.Collections.Generic;

namespace WorkflowManager.Models
{
    public partial class Comandamateriale
    {
        public int StaffId { get; set; }
        public int MaterialeId { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string Detalii { get; set; }
        public int? Cantitate { get; set; }
        public double? Pret { get; set; }

        public virtual Materiale Materiale { get; set; }
        public virtual Staff Staff { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WorkflowManager.Models;

namespace WorkflowManager.Controllers
{
    public class EchipamentsController : Controller
    {
        private readonly workflow_managerContext _context;

        public EchipamentsController(workflow_managerContext context)
        {
            _context = context;
        }

        // GET: Echipaments
        public async Task<IActionResult> Index()
        {
            var workflow_managerContext = _context.Echipament.Include(e => e.Echipa);
            return View(await workflow_managerContext.ToListAsync());
        }

        // GET: Echipaments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var echipament = await _context.Echipament
                .Include(e => e.Echipa)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (echipament == null)
            {
                return NotFound();
            }

            return View(echipament);
        }

        // GET: Echipaments/Create
        public IActionResult Create()
        {
            ViewData["EchipaId"] = new SelectList(_context.Echipa, "Id", "Nume");
            return View();
        }

        // POST: Echipaments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nume,Detalii,Pret,CreateTime,UpdateTime,EchipaId")] Echipament echipament)
        {
            if (ModelState.IsValid)
            {
                _context.Add(echipament);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EchipaId"] = new SelectList(_context.Echipa, "Id", "Nume", echipament.EchipaId);
            return View(echipament);
        }

        // GET: Echipaments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var echipament = await _context.Echipament.FindAsync(id);
            if (echipament == null)
            {
                return NotFound();
            }
            ViewData["EchipaId"] = new SelectList(_context.Echipa, "Id", "Nume", echipament.EchipaId);
            return View(echipament);
        }

        // POST: Echipaments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nume,Detalii,Pret,CreateTime,UpdateTime,EchipaId")] Echipament echipament)
        {
            if (id != echipament.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    echipament.UpdateTime = DateTime.Now;
                    _context.Update(echipament);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EchipamentExists(echipament.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EchipaId"] = new SelectList(_context.Echipa, "Id", "Nume", echipament.EchipaId);
            return View(echipament);
        }

        // GET: Echipaments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var echipament = await _context.Echipament
                .Include(e => e.Echipa)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (echipament == null)
            {
                return NotFound();
            }

            return View(echipament);
        }

        // POST: Echipaments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var echipament = await _context.Echipament.FindAsync(id);
            _context.Echipament.Remove(echipament);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EchipamentExists(int id)
        {
            return _context.Echipament.Any(e => e.Id == id);
        }
    }
}

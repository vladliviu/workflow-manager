﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WorkflowManager.Models;

namespace WorkflowManager.Controllers
{
    public class ClientsController : Controller
    {
        private readonly workflow_managerContext _context;

        public ClientsController(workflow_managerContext context)
        {
            _context = context;
        }

        public IActionResult Login()
        {
            // TempData.Keep("staffoccupationID");
            TempData["authUsername"] = "";
            TempData["authId"] = 0;
            TempData["staffoccupationID"] = 0;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(String Username, String Password)
        {
            var loginInfo = _context.Client.Where(s => s.Username == Username && s.Password == Password).FirstOrDefault();
            var staffLoginInfo = _context.Staff.Where(s => s.Username == Username && s.Password == Password).FirstOrDefault();

            if (loginInfo == null && staffLoginInfo == null)
            {
                return (RedirectToAction(nameof(Login)));
            }
            else if (loginInfo != null)
            {
                TempData["authUsername"] = loginInfo.Username;
                TempData["authId"] = loginInfo.Id;
                TempData["staffoccupationID"] = 4;
                return RedirectToAction("Details", new { id = loginInfo.Id });
            }
            else
            {
                TempData["authUsername"] = staffLoginInfo.Username;
                TempData["authId"] = staffLoginInfo.Id;
                TempData["staffoccupationID"] = staffLoginInfo.FunctiestaffId;
                return RedirectToAction("Details", "Staffs", new { id = staffLoginInfo.Id });
            }
        }

        public IActionResult Logout()
        {
            TempData["authUsername"] = "";
            TempData["authId"] = 0;
            TempData["staffoccupationID"] = 0;
            return RedirectToAction(nameof(Login));
        }

        public IActionResult Register()
        {
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("Id,Username,Password,Nume,Prenume,Email,Telefon,Adresa,Avatar,CreateTime,UpdateTime")] Client client, String confirmPassword)
        {
            if (ModelState.IsValid && client.Password == confirmPassword)
            {
                _context.Add(client);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Login));
            }
            else if (client.Password != confirmPassword)
            {
                return RedirectToAction(nameof(Register));
            }
            return View(client);
        }

        // GET: Clients
        public async Task<IActionResult> Index()
        {
            return View(await _context.Client.ToListAsync());
        }

        // GET: Clients/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await _context.Client
                .FirstOrDefaultAsync(m => m.Id == id);
            if (client == null)
            {
                return NotFound();
            }

            return View(client);
        }

        // GET: Clients/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Username,Password,Nume,Prenume,Email,Telefon,Adresa,Avatar,CreateTime,UpdateTime")] Client client)
        {
            if (ModelState.IsValid)
            {
                _context.Add(client);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(client);
        }

        // GET: Clients/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await _context.Client.FindAsync(id);
            if (client == null)
            {
                return NotFound();
            }
            return View(client);
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Username,Password,Nume,Prenume,Email,Telefon,Adresa,Avatar,CreateTime,UpdateTime")] Client client)
        {
            if (id != client.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    client.UpdateTime = DateTime.Now;
                    _context.Update(client);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientExists(client.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(client);
        }

        // GET: Clients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await _context.Client
                .FirstOrDefaultAsync(m => m.Id == id);
            if (client == null)
            {
                return NotFound();
            }

            return View(client);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var client = await _context.Client.FindAsync(id);
            _context.Client.Remove(client);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClientExists(int id)
        {
            return _context.Client.Any(e => e.Id == id);
        }
    }
}

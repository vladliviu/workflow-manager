﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WorkflowManager.Models;

namespace WorkflowManager.Controllers
{
    public class ComandamaterialesController : Controller
    {
        private readonly workflow_managerContext _context;

        public ComandamaterialesController(workflow_managerContext context)
        {
            _context = context;
        }

        // GET: Comandamateriales
        public async Task<IActionResult> Index()
        {
            var workflow_managerContext = _context.Comandamateriale.Include(c => c.Materiale).Include(c => c.Staff);
            return View(await workflow_managerContext.ToListAsync());
        }

        // GET: Comandamateriales/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comandamateriale = await _context.Comandamateriale
                .Include(c => c.Materiale)
                .Include(c => c.Staff)
                .FirstOrDefaultAsync(m => m.StaffId == id);
            if (comandamateriale == null)
            {
                return NotFound();
            }

            return View(comandamateriale);
        }

        // GET: Comandamateriales/Create
        public IActionResult Create()
        {
            ViewData["MaterialeId"] = new SelectList(_context.Materiale, "Id", "Nume");
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username");
            return View();
        }

        // POST: Comandamateriales/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StaffId,MaterialeId,CreateTime,UpdateTime,Detalii,Cantitate,Pret")] Comandamateriale comandamateriale)
        {
            if (ModelState.IsValid)
            {
                _context.Add(comandamateriale);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MaterialeId"] = new SelectList(_context.Materiale, "Id", "Nume", comandamateriale.MaterialeId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", comandamateriale.StaffId);
            return View(comandamateriale);
        }

        // GET: Comandamateriales/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comandamateriale = await _context.Comandamateriale.FindAsync(id);
            if (comandamateriale == null)
            {
                return NotFound();
            }
            ViewData["MaterialeId"] = new SelectList(_context.Materiale, "Id", "Nume", comandamateriale.MaterialeId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", comandamateriale.StaffId);
            return View(comandamateriale);
        }

        // POST: Comandamateriales/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StaffId,MaterialeId,CreateTime,UpdateTime,Detalii,Cantitate,Pret")] Comandamateriale comandamateriale)
        {
            if (id != comandamateriale.StaffId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    comandamateriale.UpdateTime = DateTime.Now;
                    _context.Update(comandamateriale);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ComandamaterialeExists(comandamateriale.StaffId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MaterialeId"] = new SelectList(_context.Materiale, "Id", "Nume", comandamateriale.MaterialeId);
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", comandamateriale.StaffId);
            return View(comandamateriale);
        }

        // GET: Comandamateriales/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comandamateriale = await _context.Comandamateriale
                .Include(c => c.Materiale)
                .Include(c => c.Staff)
                .FirstOrDefaultAsync(m => m.StaffId == id);
            if (comandamateriale == null)
            {
                return NotFound();
            }

            return View(comandamateriale);
        }

        // POST: Comandamateriales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var comandamateriale = await _context.Comandamateriale.FindAsync(id);
            _context.Comandamateriale.Remove(comandamateriale);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ComandamaterialeExists(int id)
        {
            return _context.Comandamateriale.Any(e => e.StaffId == id);
        }
    }
}

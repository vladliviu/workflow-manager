﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WorkflowManager.Models;

namespace WorkflowManager.Controllers
{
    public class ProiectactivsController : Controller
    {
        private readonly workflow_managerContext _context;

        public ProiectactivsController(workflow_managerContext context)
        {
            _context = context;
        }

        // GET: Proiectactivs
        public async Task<IActionResult> Index()
        {

            if (TempData["staffoccupationID"].ToString() == "2" || TempData["staffoccupationID"].ToString() == "3")
            {
                var workflow_managerContext = _context.Proiectactiv.Include(p => p.Client).Include(p => p.Echipa).Include(p => p.Proiect).Include(p => p.Status);
                return View(await workflow_managerContext.ToListAsync());
            }
            else if (TempData["staffoccupationID"].ToString() == "4")
            {
                var workflow_managerContext = _context.Proiectactiv.Include(p => p.Client).Include(p => p.Echipa).Include(p => p.Proiect).Include(p => p.Status).Where(p => p.ClientId == (int)TempData["authId"]);
                return View(await workflow_managerContext.ToListAsync());
            }
            else return NotFound();
           
        }

        // GET: Proiectactivs/Details/5
        public async Task<IActionResult> Details(int? id, int? idC, int? idE, int? idS)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proiectactiv = await _context.Proiectactiv
                .Include(p => p.Client)
                .Include(p => p.Echipa)
                .Include(p => p.Proiect)
                .Include(p => p.Status)
                .FirstOrDefaultAsync(m => m.ProiectId == id && idC == m.ClientId && idE == m.EchipaId && idS == m.StatusId);
            if (proiectactiv == null)
            {
                return NotFound();
            }

            return View(proiectactiv);
        }

        // GET: Proiectactivs/Create
        public IActionResult Create()
        {
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Username");
            ViewData["EchipaId"] = new SelectList(_context.Echipa, "Id", "Nume");
            ViewData["ProiectId"] = new SelectList(_context.Proiect, "Id", "Nume");
            ViewData["StatusId"] = new SelectList(_context.Status, "Id", "Nume");
            return View();
        }

        // POST: Proiectactivs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProiectId,ClientId,CreateTime,UpdateTime,Start,End,EchipaId,StatusId")] Proiectactiv proiectactiv)
        {
            if (ModelState.IsValid)
            {
                _context.Add(proiectactiv);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Username", proiectactiv.ClientId);
            ViewData["EchipaId"] = new SelectList(_context.Echipa, "Id", "Nume", proiectactiv.EchipaId);
            ViewData["ProiectId"] = new SelectList(_context.Proiect, "Id", "Nume", proiectactiv.ProiectId);
            ViewData["StatusId"] = new SelectList(_context.Status, "Id", "Nume", proiectactiv.StatusId);
            return View(proiectactiv);
        }

        // GET: Proiectactivs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proiectactiv = await _context.Proiectactiv.FindAsync(id);
            if (proiectactiv == null)
            {
                return NotFound();
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Username", proiectactiv.ClientId);
            ViewData["EchipaId"] = new SelectList(_context.Echipa, "Id", "Nume", proiectactiv.EchipaId);
            ViewData["ProiectId"] = new SelectList(_context.Proiect, "Id", "Nume", proiectactiv.ProiectId);
            ViewData["StatusId"] = new SelectList(_context.Status, "Id", "Nume", proiectactiv.StatusId);
            return View(proiectactiv);
        }

        // POST: Proiectactivs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, [Bind("ProiectId,ClientId,CreateTime,UpdateTime,Start,End,EchipaId,StatusId")] Proiectactiv proiectactiv)
        {
            if (id != proiectactiv.ProiectId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    proiectactiv.UpdateTime = DateTime.Now;
                    _context.Update(proiectactiv);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProiectactivExists(proiectactiv.ProiectId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.Client, "Id", "Username", proiectactiv.ClientId);
            ViewData["EchipaId"] = new SelectList(_context.Echipa, "Id", "Nume", proiectactiv.EchipaId);
            ViewData["ProiectId"] = new SelectList(_context.Proiect, "Id", "Nume", proiectactiv.ProiectId);
            ViewData["StatusId"] = new SelectList(_context.Status, "Id", "Nume", proiectactiv.StatusId);
            return View(proiectactiv);
        }

        // GET: Proiectactivs/Delete/5
        public async Task<IActionResult> Delete(int? id, int? idC, int? idE, int? idS)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proiectactiv = await _context.Proiectactiv
                .Include(p => p.Client)
                .Include(p => p.Echipa)
                .Include(p => p.Proiect)
                .Include(p => p.Status)
                .FirstOrDefaultAsync(m => m.ProiectId == id && idC == m.ClientId && idE == m.EchipaId && idS == m.StatusId);
            if (proiectactiv == null)
            {
                return NotFound();
            }

            return View(proiectactiv);
        }

        // POST: Proiectactivs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var proiectactiv = await _context.Proiectactiv.FindAsync(id);
            _context.Proiectactiv.Remove(proiectactiv);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProiectactivExists(int id)
        {
            return _context.Proiectactiv.Any(e => e.ProiectId == id);
        }
    }
}

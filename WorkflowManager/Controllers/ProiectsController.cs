﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WorkflowManager.Models;

namespace WorkflowManager.Controllers
{
    public class ProiectsController : Controller
    {
        private readonly workflow_managerContext _context;

        public ProiectsController(workflow_managerContext context)
        {
            _context = context;
        }

        // GET: Proiects
        public async Task<IActionResult> Index()
        {
            return View(await _context.Proiect.ToListAsync());
        }

        // GET: Proiects/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proiect = await _context.Proiect
                .FirstOrDefaultAsync(m => m.Id == id);
            if (proiect == null)
            {
                return NotFound();
            }

            return View(proiect);
        }

        // GET: Proiects/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Proiects/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nume,Detalii,Status,Valoare,Finantare,Avatar,CreateTime,UpdateTime")] Proiect proiect)
        {
            if (ModelState.IsValid)
            {
                _context.Add(proiect);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(proiect);
        }

        // GET: Proiects/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proiect = await _context.Proiect.FindAsync(id);
            if (proiect == null)
            {
                return NotFound();
            }
            return View(proiect);
        }

        // POST: Proiects/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nume,Detalii,Status,Valoare,Finantare,Avatar,CreateTime,UpdateTime")] Proiect proiect)
        {
            if (id != proiect.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    proiect.UpdateTime = DateTime.Now;
                    _context.Update(proiect);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProiectExists(proiect.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(proiect);
        }

        // GET: Proiects/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proiect = await _context.Proiect
                .FirstOrDefaultAsync(m => m.Id == id);
            if (proiect == null)
            {
                return NotFound();
            }

            return View(proiect);
        }

        // POST: Proiects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var proiect = await _context.Proiect.FindAsync(id);
            _context.Proiect.Remove(proiect);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProiectExists(int id)
        {
            return _context.Proiect.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WorkflowManager.Models;

namespace WorkflowManager.Controllers
{
    public class FunctiestaffsController : Controller
    {
        private readonly workflow_managerContext _context;

        public FunctiestaffsController(workflow_managerContext context)
        {
            _context = context;
        }

        // GET: Functiestaffs
        public async Task<IActionResult> Index()
        {
            return View(await _context.Functiestaff.ToListAsync());
        }

        // GET: Functiestaffs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var functiestaff = await _context.Functiestaff
                .FirstOrDefaultAsync(m => m.Id == id);
            if (functiestaff == null)
            {
                return NotFound();
            }

            return View(functiestaff);
        }

        // GET: Functiestaffs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Functiestaffs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nume,Descriere,Salariu,Bonus,Valoaretichet,Numartichet,CreateTime,UpdateTime")] Functiestaff functiestaff)
        {
            if (ModelState.IsValid)
            {
                _context.Add(functiestaff);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(functiestaff);
        }

        // GET: Functiestaffs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var functiestaff = await _context.Functiestaff.FindAsync(id);
            if (functiestaff == null)
            {
                return NotFound();
            }
            return View(functiestaff);
        }

        // POST: Functiestaffs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nume,Descriere,Salariu,Bonus,Valoaretichet,Numartichet,CreateTime,UpdateTime")] Functiestaff functiestaff)
        {
            if (id != functiestaff.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    functiestaff.UpdateTime = DateTime.Now;
                    _context.Update(functiestaff);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FunctiestaffExists(functiestaff.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(functiestaff);
        }

        // GET: Functiestaffs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var functiestaff = await _context.Functiestaff
                .FirstOrDefaultAsync(m => m.Id == id);
            if (functiestaff == null)
            {
                return NotFound();
            }

            return View(functiestaff);
        }

        // POST: Functiestaffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var functiestaff = await _context.Functiestaff.FindAsync(id);
            _context.Functiestaff.Remove(functiestaff);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FunctiestaffExists(int id)
        {
            return _context.Functiestaff.Any(e => e.Id == id);
        }
    }
}

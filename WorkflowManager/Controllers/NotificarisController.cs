﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WorkflowManager.Models;

namespace WorkflowManager.Controllers
{
    public class NotificarisController : Controller
    {
        private readonly workflow_managerContext _context;

        public NotificarisController(workflow_managerContext context)
        {
            _context = context;
        }

        // GET: Notificaris
        public async Task<IActionResult> Index()
        {
            var workflow_managerContext = _context.Notificari.Include(n => n.Staff);
            return View(await workflow_managerContext.ToListAsync());
        }

        // GET: Notificaris/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificari = await _context.Notificari
                .Include(n => n.Staff)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (notificari == null)
            {
                return NotFound();
            }

            return View(notificari);
        }

        // GET: Notificaris/Create
        public IActionResult Create()
        {
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username");
            return View();
        }

        // POST: Notificaris/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nume,Tip,Descriere,Status,CreateTime,UpdateTime,StaffId")] Notificari notificari)
        {
            if (ModelState.IsValid)
            {
                _context.Add(notificari);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", notificari.StaffId);
            return View(notificari);
        }

        // GET: Notificaris/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificari = await _context.Notificari.FindAsync(id);
            if (notificari == null)
            {
                return NotFound();
            }
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", notificari.StaffId);
            return View(notificari);
        }

        // POST: Notificaris/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nume,Tip,Descriere,Status,CreateTime,UpdateTime,StaffId")] Notificari notificari)
        {
            if (id != notificari.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    notificari.UpdateTime = DateTime.Now;
                    _context.Update(notificari);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NotificariExists(notificari.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StaffId"] = new SelectList(_context.Staff, "Id", "Username", notificari.StaffId);
            return View(notificari);
        }

        // GET: Notificaris/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificari = await _context.Notificari
                .Include(n => n.Staff)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (notificari == null)
            {
                return NotFound();
            }

            return View(notificari);
        }

        // POST: Notificaris/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var notificari = await _context.Notificari.FindAsync(id);
            _context.Notificari.Remove(notificari);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NotificariExists(int id)
        {
            return _context.Notificari.Any(e => e.Id == id);
        }
    }
}
